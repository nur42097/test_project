import { Card, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { IMovieInfo, IMovieInfoParams } from "src/interfaces";
import { getMovieById } from "src/services/movieApi";

export const MovieInfo: React.FC = () => {

    const movieParams = useParams<{ id: string }>();
    const [movieInfo, setMovieInfo] = useState<IMovieInfo>()

    useEffect(() => {
        getMovieInfo()
    }, [])

    const getMovieInfo = () => {
        getMovieById(movieParams.id)
            .then(res => {
                if (res.Response === "True") {
                    setMovieInfo(res)
                }
            })
    }

    return (
        <Grid container>
            <Card style={{width: "100%"}} >
                <CardMedia
                    component="img"
                    alt="green iguana"
                    height="340"
                    src={movieInfo?.Poster}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {movieInfo?.Title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {movieInfo?.Awards}
                    </Typography>
                </CardContent>
            </Card>
        </Grid>
    )
}

export default MovieInfo