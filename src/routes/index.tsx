import React from 'react';
import { Navigate, RouteObject } from 'react-router-dom';
import MoviesList from 'routes/moviesList';
import MovieInfo from 'routes/movieInfo';

const routes: RouteObject[] = [
    {
        path: '/',
        element: <Navigate to="movies" />,
    },
    {
        path: '/movies',
        element: <MoviesList />
    },
    {
        path: '/movie/:id',
        element: <MovieInfo />,
    },
];


export default routes