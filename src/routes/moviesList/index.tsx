import { Box, CircularProgress, FormControl, Grid, IconButton, Input, InputAdornment, InputLabel, TextField } from "@mui/material";
import React, { useEffect, useState } from "react";
import MovieCard from "src/components/movieCard";
import useDebounce from "src/hooks/useDebounce";
import { searchMovies } from "src/services/movieApi";
import { IMovie } from "src/interfaces";

export const MoviesList: React.FC = () => {

    const [searchText, setSearchText] = useState("")
    const handleChangeSearchText = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => setSearchText(e.target.value)
    const [results, setResults] = useState<IMovie[]>([]);
    const [isSearching, setIsSearching] = useState(false);

    const debouncedSearchText = useDebounce(searchText, 500);

    useEffect(() => {
        if (debouncedSearchText && debouncedSearchText.length > 3) {
            setIsSearching(true);
            searchMovies(debouncedSearchText)
                .then(result => {
                    setIsSearching(false);
                    if (result.Response === "True") {
                        setResults(result.Search)
                    } else {
                        setResults([])
                    }
                });
        } else {
            setResults([]);
        }
    },
        [debouncedSearchText]
    );

    return (
        <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12}>
                <Box
                    component="form"
                    noValidate
                    autoComplete="off"
                >
                    <FormControl fullWidth sx={{ m: 1 }} variant="standard">
                        <InputLabel htmlFor="standard-adornment-amount">Фильм</InputLabel>
                        <Input
                            id="standard-adornment-amount"
                            value={searchText}
                            onChange={handleChangeSearchText}
                        />
                    </FormControl>
                </Box>
            </Grid>
            {
                isSearching ?
                    <Box sx={{ display: 'flex', width: "100%", height: "200px", justifyContent: "center", alignItems: "center" }}>
                        <CircularProgress />
                    </Box> :
                    results.map((item, ind) => (
                        <MovieCard key={ind} movieData={item} />
                    ))
            }
        </Grid>
    )
}

export default MoviesList