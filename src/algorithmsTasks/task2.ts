/*
   У вас есть авиабилеты пассажира, по которым он летал.
   Вам нужно построить его маршрут.
 
   Input:
   [
       ['Moscow', 'New-York'],
       ['Nur-Sultan', 'Moscow'],
       ['New-York', 'Almaty']
   ]
   Output: ['Nur-Sultan', 'Moscow', 'New-York', 'Almaty'],
*/

export function getPassengerRoutes(routes: (string[])[]): string[] {
    let result: string[] = []

    for (let routeItemIndex in routes) {
        let routeItem = routes[routeItemIndex]
        if (checkRouteAbsenceFromOtherRoute(routes, routeItem[0])) {
            result[0] = routeItem[0]
        }
    }

    routes.forEach(() => {
        const foundItem = findSecondRouteFromFirstRoute(routes, result[result.length - 1]) 
        result.push(foundItem)
    })
    console.log(result)
    return result
}

export function checkRouteAbsenceFromOtherRoute(routes: (string[])[], routeValue: string) {
    let foundData = routes.find(el => el[el.length - 1] === routeValue)
    if (!foundData) return true
    return false
}

export function findSecondRouteFromFirstRoute(routes: (string[])[], firstRoute: string) {
    let foundData = routes.find(el => el[0] === firstRoute)
    if (foundData) {
        return foundData[foundData.length - 1]
    }
    return "Not Found"
}