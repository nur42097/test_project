/*
   Задан массив целых чисел.
   Найдите минимальный положительное число,
   которое не встречается в этом массиве
 
   Input: nums = [1,2,0]
   Output: 3
 
   Input: nums = [3,4,-1,1]
   Output: 2
 
   Input: nums = [7,8,9,11,12]
   Output: 1
*/

export function findMinPositiveNumberWhereNotContainsArray(array: number[], minPositiveNumber: number): number {
    if (!array.includes(minPositiveNumber)) {
        return minPositiveNumber
    } else {
        return findMinPositiveNumberWhereNotContainsArray(array, minPositiveNumber + 1)
    }
}

export const getMinNum = (nums: number[]) => {
    const output = findMinPositiveNumberWhereNotContainsArray(nums, 1)
    console.log(output)
}
