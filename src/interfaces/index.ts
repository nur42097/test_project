import { OmdbResponceType } from "src/types";

export interface IErrorBoundaryState {
    hasError: boolean;
    error: Error;
}

export interface IErrorBoundaryProps {
    children: React.ReactNode
}

export interface IMovie {
    Title: string;
    Year: number;
    imdbID: string;
    Type: string;
    Poster: string;
}

export interface IOmdbSearchResponce {
    Search: IMovie[];
    totalResults: string;
    Response: OmdbResponceType;
}

export interface IMovieCard {
    movieData: IMovie;
}

export interface IMovieInfoParams {
    id: string;
}

export interface IMovieInfo {
    Title: string,
    Year: string,
    Rated: string,
    Released: string,
    Runtime: string,
    Genre: string,
    Director: string,
    Writer: string,
    Actors: string,
    Plot: string,
    Language: string,
    Country: string,
    Awards: string,
    Poster: string,
    Ratings: IMovieRating[],
    Metascore: string,
    imdbRating: string,
    imdbVotes: string,
    imdbID: string,
    Type: string,
    DVD: string,
    BoxOffice: string,
    Production: string,
    Website: string,
    Response: OmdbResponceType
}

export interface IMovieRating {
    Source: string,
    Value: string,
}