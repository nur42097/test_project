import { config } from 'data/index';
import Axios from "axios"
import { IMovieInfo, IOmdbSearchResponce } from 'src/interfaces';

export const searchMovies = (searchText: string): Promise<IOmdbSearchResponce> => {
    const url = `http://www.omdbapi.com/?apikey=${config.OMDB_API_KEY}&s=${searchText}`
    return new Promise((resolve, reject) => {
        Axios.get(url)
            .then(res => {
                return resolve(res.data)
            })
            .catch(() => resolve({} as IOmdbSearchResponce))
    })
}

export const getMovieById = (id: string | undefined): Promise<IMovieInfo> => {
    const url = `http://www.omdbapi.com/?apikey=${config.OMDB_API_KEY}&i=${id}`
    return new Promise((resolve, reject) => {
        Axios.get(url)
            .then(res => {
                return resolve(res.data)
            })
            .catch(() => resolve({} as IMovieInfo))
    })
}