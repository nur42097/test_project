import React from 'react'
import { useRoutes } from 'react-router'
import routes from 'src/routes'
import ErrorBoundary from 'components/boundary/ErrorBoundaryContainer'
import Container from '@mui/material/Container';
import 'src/index.css'
import { getMinNum } from 'src/algorithmsTasks/task1';
import { getPassengerRoutes } from 'src/algorithmsTasks/task2';

const App: React.FC = () => {

    const context = useRoutes(routes)

    getMinNum([7,8,9,11,12])

    getPassengerRoutes([
        ['Moscow', 'New-York'],
        ['Nur-Sultan', 'Moscow'],
        ['New-York', 'Almaty']
    ])

    return (
        <ErrorBoundary>
            <Container className='mainContainer' >
                {context}
            </Container>
        </ErrorBoundary>
    )
}

export default App