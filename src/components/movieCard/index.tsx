import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Button, CardActionArea, CardActions, Grid } from '@mui/material';
import { IMovieCard } from 'src/interfaces';
import { useNavigate } from 'react-router';

const MovieCard: React.FC<IMovieCard> = ({
    movieData: {
        Title, Year, Poster, imdbID
    }
}) => {

    const navigate = useNavigate()

    const hanldeOpenMovie = () => {
        navigate(`/movie/${imdbID}`)
    }

    return (
        <Grid item xs={2} sm={4} md={4}>
            <Card onClick={hanldeOpenMovie} >
                <CardActionArea>
                    <CardMedia
                        component="img"
                        height="140"
                        image={Poster}
                        alt="green iguana"
                    />
                    <CardContent style={{height: "100px"}} >
                        <Typography gutterBottom variant="h5" component="div">
                            {Title}  {Year}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        </Grid>
    );
}

export default MovieCard
